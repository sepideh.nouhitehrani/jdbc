package view;

import controller.UserController;
import controller.ProductController;
import controller.OrderController;
import model.User;
import model.Product;
import model.Order;

import java.util.List;
import java.util.Scanner;

public class Main {
    private static final Scanner scanner = new Scanner(System.in);
    private static final UserController userController = new UserController();
    private static final ProductController productController = new ProductController();
    private static final OrderController orderController = new OrderController();

    public static void main(String[] args) {
        try {
            while (true) {
                displayMenu();
                int choice = scanner.nextInt();
                scanner.nextLine(); // Consume newline

                switch (choice) {
                    case 1:
                        displayAllUsers();
                        break;
                    case 2:
                        insertUser();
                        break;
                    case 3:
                        updateUser();
                        break;
                    case 4:
                        deleteUser();
                        break;
                    case 5:
                        displayUserById();
                        break;
                    case 6:
                        displayAllProducts();
                        break;
                    case 7:
                        insertProduct();
                        break;
                    case 8:
                        updateProduct();
                        break;
                    case 9:
                        deleteProduct();
                        break;
                    case 10:
                        displayAllOrders();
                        break;
                    case 11:
                        insertOrder();
                        break;
                    case 12:
                        updateOrder();
                        break;
                    case 13:
                        deleteOrder();
                        break;
                    case 14:
                        displayTotalPriceOfOrders();
                        break;
                    case 15:
                        finish();
                        break;
                    default:
                        System.out.println("Invalid choice. Please try again.");
                }

                System.out.println(); // Empty line for readability
            }
        } catch (Exception e) {
            System.err.println("An error occurred: " + e.getMessage());
        } finally {
            scanner.close(); // Close the scanner when the program finishes
        }
    }

    private static void displayMenu() {
        System.out.println("----- Menu -----");
        System.out.println("1. Show all users");
        System.out.println("2. Insert a user");
        System.out.println("3. Update a user");
        System.out.println("4. Delete a user");
        System.out.println("5. Display user by ID");
        System.out.println("6. Show all products");
        System.out.println("7. Insert a product");
        System.out.println("8. Update a product");
        System.out.println("9. Delete a product");
        System.out.println("10. Show all orders");
        System.out.println("11. Insert an order");
        System.out.println("12. Update an order");
        System.out.println("13. Delete an order");
        System.out.println("14. Show total price of all orders"); // Option to display total price of all orders
        System.out.println("15. Finish"); // Option to finish the program
        System.out.print("Enter your choice: ");
    }

    // Methods for user operations
    private static void displayAllUsers() {
        List<User> userList = userController.getAllUsers();
        if (userList.isEmpty()) {
            System.out.println("No users found.");
        } else {
            System.out.println("----- All Users -----");
            for (User user : userList) {
                System.out.println(user);
            }
        }
    }

    private static void insertUser() {
        System.out.print("Enter username: ");
        String username = scanner.nextLine();
        System.out.print("Enter email: ");
        String email = scanner.nextLine();
        System.out.print("Enter password: ");
        String password = scanner.nextLine();
       System.out.print("Enter full name: ");
       String fullName = scanner.nextLine();
        System.out.print("Enter age: ");
        int age = scanner.nextInt();
        scanner.nextLine(); // Consume newline
        System.out.print("Enter gender (Male/Female/Other): ");
        String gender = scanner.nextLine();

        User newUser = new User(0, username, email, password ,fullName, age, gender);
        userController.insertUser(newUser);
        System.out.println("User inserted successfully.");
    }

    private static void updateUser() {
        System.out.print("Enter user ID to update: ");
        int userId = scanner.nextInt();
        scanner.nextLine(); // Consume newline
        User existingUser = userController.getUserById(userId);

        if (existingUser != null) {
            System.out.print("Enter new username: ");
            String newUsername = scanner.nextLine();
            System.out.print("Enter new email: ");
            String newEmail = scanner.nextLine();
            System.out.print("Enter new password: ");
            String newPassword = scanner.nextLine();
            System.out.print("Enter new full name: ");
            String newFullName = scanner.nextLine();
            System.out.print("Enter new age: ");
            int newAge = scanner.nextInt();
            scanner.nextLine(); // Consume newline
            System.out.print("Enter new gender (Male/Female/Other): ");
            String newGender = scanner.nextLine();

            existingUser.setUsername(newUsername);
            existingUser.setEmail(newEmail);
            existingUser.setPassword(newPassword);
            existingUser.setFullName(newFullName);
            existingUser.setAge(newAge);
            existingUser.setGender(newGender);

            userController.updateUser(existingUser);
            System.out.println("User updated successfully.");
        } else {
            System.out.println("User not found.");
        }
    }

    private static void deleteUser() {
        System.out.print("Enter user ID to delete: ");
        int userId = scanner.nextInt();
        scanner.nextLine(); // Consume newline

        User existingUser = userController.getUserById(userId);
        if (existingUser != null) {
            userController.deleteUserById(userId);
            System.out.println("User deleted successfully.");
        } else {
            System.out.println("User not found.");
        }
    }

    private static void displayUserById() {
        System.out.print("Enter user ID to display: ");
        int userId = scanner.nextInt();
        scanner.nextLine(); // Consume newline

        User user = userController.getUserById(userId);
        if (user != null) {
            System.out.println(user);
        } else {
            System.out.println("User not found.");
        }
    }

    // Methods for product operations
    private static void displayAllProducts() {
        List<Product> productList = productController.getAllProducts();
        if (productList.isEmpty()) {
            System.out.println("No products found.");
        } else {
            System.out.println("----- All Products -----");
            for (Product product : productList) {
                System.out.println(product);
            }
        }
    }

    private static void insertProduct() {
        System.out.print("Enter product name: ");
        String productName = scanner.nextLine();
        System.out.print("Enter description: ");
        String description = scanner.nextLine();
        System.out.print("Enter price: ");
        double price = scanner.nextDouble();
        scanner.nextLine(); // Consume newline
        System.out.print("Enter category: ");
        String category = scanner.nextLine();
        System.out.print("Enter stock quantity: ");
        int stockQuantity = scanner.nextInt();
        scanner.nextLine(); // Consume newline
        System.out.print("Enter manufacturer: ");
        String manufacturer = scanner.nextLine();

        Product newProduct = new Product(0, productName, description, price, category, stockQuantity, manufacturer);
        productController.insertProduct(newProduct);
        System.out.println("Product inserted successfully.");
    }

    private static void updateProduct() {
        System.out.print("Enter product ID to update: ");
        int productId = scanner.nextInt();
        scanner.nextLine(); // Consume newline
        Product existingProduct = productController.getProductById(productId);

        if (existingProduct != null) {
            System.out.print("Enter new product name: ");
            String newProductName = scanner.nextLine();
            System.out.print("Enter new description: ");
            String newDescription = scanner.nextLine();
            System.out.print("Enter new price: ");
            double newPrice = scanner.nextDouble();
            scanner.nextLine(); // Consume newline
            System.out.print("Enter new category: ");
            String newCategory = scanner.nextLine();
            System.out.print("Enter new stock quantity: ");
            int newStockQuantity = scanner.nextInt();
            scanner.nextLine(); // Consume newline
            System.out.print("Enter new manufacturer: ");
            String newManufacturer = scanner.nextLine();

            existingProduct.setProductName(newProductName);
            existingProduct.setDescription(newDescription);
            existingProduct.setPrice(newPrice);
            existingProduct.setCategory(newCategory);
            existingProduct.setStockQuantity(newStockQuantity);
            existingProduct.setManufacturer(newManufacturer);

            productController.updateProduct(existingProduct);
            System.out.println("Product updated successfully.");
        } else {
            System.out.println("Product not found.");
        }
    }


    private static void deleteProduct() {
        System.out.print("Enter product ID to delete: ");
        int productId = scanner.nextInt();
        scanner.nextLine(); // Consume newline

        Product existingProduct = productController.getProductById(productId);
        if (existingProduct != null) {
            productController.deleteProductById(productId);
            System.out.println("Product deleted successfully.");
        } else {
            System.out.println("Product not found.");
        }
    }


    // Methods for order operations
    private static void displayAllOrders() {
        List<Order> orderList = orderController.getAllOrders();
        if (orderList.isEmpty()) {
            System.out.println("No orders found.");
        } else {
            System.out.println("----- All Orders -----");
            for (Order order : orderList) {
                System.out.println(order);
            }
        }
    }

    private static void insertOrder() {
        System.out.print("Enter user ID: ");
        int userId = scanner.nextInt();
        scanner.nextLine(); // Consume newline
        System.out.print("Enter product ID: ");
        int productId = scanner.nextInt();
        scanner.nextLine(); // Consume newline
        System.out.print("Enter quantity: ");
        int quantity = scanner.nextInt();
        scanner.nextLine(); // Consume newline
        System.out.print("Enter total price: ");
        double totalPrice = scanner.nextDouble();
        scanner.nextLine(); // Consume newline

        Order newOrder = new Order(0, userId, productId, null, quantity, totalPrice);
        orderController.insertOrder(newOrder);
        System.out.println("Order inserted successfully.");
    }


    private static void updateOrder() {
        System.out.print("Enter order ID to update: ");
        int orderId = scanner.nextInt();
        scanner.nextLine(); // Consume newline
        Order existingOrder = orderController.getOrderById(orderId);

        if (existingOrder != null) {
            System.out.print("Enter new user ID: ");
            int newUserId = scanner.nextInt();
            scanner.nextLine(); // Consume newline
            System.out.print("Enter new product ID: ");
            int newProductId = scanner.nextInt();
            scanner.nextLine(); // Consume newline
            System.out.print("Enter new quantity: ");
            int newQuantity = scanner.nextInt();
            scanner.nextLine(); // Consume newline
            System.out.print("Enter new total price: ");
            double newTotalPrice = scanner.nextDouble();
            scanner.nextLine(); // Consume newline

            existingOrder.setUserId(newUserId);
            existingOrder.setProductId(newProductId);
            existingOrder.setQuantity(newQuantity);
            existingOrder.setTotalPrice(newTotalPrice);

            orderController.updateOrder(existingOrder);
            System.out.println("Order updated successfully.");
        } else {
            System.out.println("Order not found.");
        }
    }


    private static void deleteOrder() {
        System.out.print("Enter order ID to delete: ");
        int orderId = scanner.nextInt();
        scanner.nextLine(); // Consume newline

        Order existingOrder = orderController.getOrderById(orderId);
        if (existingOrder != null) {
            orderController.deleteOrderById(orderId);
            System.out.println("Order deleted successfully.");
        } else {
            System.out.println("Order not found.");
        }
    }
    private static void displayTotalPriceOfOrders() {
        double totalPrice = orderController.getTotalPriceOfAllOrders();
        System.out.println("Total price of all orders: " + totalPrice);
    }

    // Method to finish the program
    private static void finish() {
        System.out.println("Exiting program...");
        System.exit(0);
    }
}
