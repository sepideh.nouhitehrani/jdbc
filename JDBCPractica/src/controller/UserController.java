package controller;

import model.User;
import model.UserDAO;

import java.util.List;

public class UserController {
    private UserDAO userDAO;

    public UserController() {
        userDAO = new UserDAO();
    }

    // Method to get all users
    public List<User> getAllUsers() {
        return userDAO.getAllUsers();
    }

    // Method to get user by ID
    public User getUserById(int id) {
        return userDAO.getUserById(id);
    }

    // Method to insert a new user
    public void insertUser(User user) {
        userDAO.insertUser(user);
    }

    // Method to update an existing user
    public void updateUser(User user) {
        userDAO.updateUser(user);
    }

    // Method to delete a user by ID
    public void deleteUserById(int id) {
        userDAO.deleteUserById(id);
    }
}
