CREATE DATABASE database_jdbc;

use database_jdbc;
CREATE TABLE users (
    user_id INT AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(50) NOT NULL,
    email VARCHAR(100) NOT NULL,
    password VARCHAR(100) NOT NULL,
    full_name VARCHAR(100),
    age INT,
    gender ENUM('Male', 'Female', 'Other')
);

CREATE TABLE products (
    product_id INT AUTO_INCREMENT PRIMARY KEY,
    product_name VARCHAR(100) NOT NULL,
    description TEXT,
    price DECIMAL(10,2) NOT NULL,
    category VARCHAR(50),
    stock_quantity INT,
    manufacturer VARCHAR(100)
);

CREATE TABLE orders (
    order_id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT,
    product_id INT,
    order_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    quantity INT,
    total_price DECIMAL(10,2),
    FOREIGN KEY (user_id) REFERENCES users(user_id),
    FOREIGN KEY (product_id) REFERENCES products(product_id)
);


-- Insert sample data into the 'users' table
INSERT INTO users (username, email, password, full_name, age, gender)
VALUES 
    ('john_doe', 'john@example.com', 'password123', 'John Doe', 30, 'Male'),
    ('jane_smith', 'jane@example.com', 'password456', 'Jane Smith', 25, 'Female');

-- Insert sample data into the 'products' table
INSERT INTO products (product_name, description, price, category, stock_quantity, manufacturer)
VALUES 
    ('Laptop', 'High-performance laptop', 1200.00, 'Electronics', 10, 'ABC Electronics'),
    ('Smartphone', 'Latest smartphone model', 800.00, 'Electronics', 20, 'XYZ Mobiles');

-- Insert sample data into the 'orders' table
INSERT INTO orders (user_id, product_id, quantity, total_price)
VALUES 
    (1, 1, 2, 2400.00),
    (2, 2, 1, 800.00);
