package controller;

import model.Product;
import model.ProductDAO;
import java.util.List;

public class ProductController {
    private final ProductDAO productDAO;

    public ProductController() {
        this.productDAO = new ProductDAO();
    }

    public List<Product> getAllProducts() {
        return productDAO.getAllProducts();
    }

    public Product getProductById(int productId) {
        return productDAO.getProductById(productId);
    }

    public void insertProduct(Product product) {
        productDAO.insertProduct(product);
    }

    public void updateProduct(Product product) {
        productDAO.updateProduct(product);
    }

    public void deleteProductById(int productId) {
        productDAO.deleteProductById(productId);
    }
}
