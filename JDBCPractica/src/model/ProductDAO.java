package model;

import connection.ConnectionDB;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ProductDAO {
    private Connection connection;

    public ProductDAO() {
        ConnectionDB connectionDB = new ConnectionDB();
        connection = connectionDB.getConnection();
    }

    // Method to retrieve all products
    public List<Product> getAllProducts() {
        List<Product> productList = new ArrayList<>();
        String sql = "SELECT * FROM products";

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int productId = resultSet.getInt("product_id");
                String productName = resultSet.getString("product_name");
                String description = resultSet.getString("description");
                double price = resultSet.getDouble("price");
                String category = resultSet.getString("category");
                int stockQuantity = resultSet.getInt("stock_quantity");
                String manufacturer = resultSet.getString("manufacturer");
                productList.add(new Product(productId, productName, description, price, category, stockQuantity, manufacturer));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return productList;
    }

    public Product getProductById(int productId) {
        String query = "SELECT * FROM products WHERE product_id = ?";
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setInt(1, productId);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                int id = resultSet.getInt("product_id");
                String productName = resultSet.getString("product_name");
                String description = resultSet.getString("description");
                double price = resultSet.getDouble("price");
                String category = resultSet.getString("category");
                int stockQuantity = resultSet.getInt("stock_quantity");
                String manufacturer = resultSet.getString("manufacturer");

                return new Product(id, productName, description, price, category, stockQuantity, manufacturer);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null; // Return null if product with given ID is not found
    }
    // Method to insert a new product
    public void insertProduct(Product product) {
        String sql = "INSERT INTO products (product_name, description, price, category, stock_quantity, manufacturer) VALUES (?, ?, ?, ?, ?, ?)";

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, product.getProductName());
            preparedStatement.setString(2, product.getDescription());
            preparedStatement.setDouble(3, product.getPrice());
            preparedStatement.setString(4, product.getCategory());
            preparedStatement.setInt(5, product.getStockQuantity());
            preparedStatement.setString(6, product.getManufacturer());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // Method to update an existing product
    public void updateProduct(Product product) {
        String sql = "UPDATE products SET product_name = ?, description = ?, price = ?, category = ?, stock_quantity = ?, manufacturer = ? WHERE product_id = ?";

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, product.getProductName());
            preparedStatement.setString(2, product.getDescription());
            preparedStatement.setDouble(3, product.getPrice());
            preparedStatement.setString(4, product.getCategory());
            preparedStatement.setInt(5, product.getStockQuantity());
            preparedStatement.setString(6, product.getManufacturer());
            preparedStatement.setInt(7, product.getProductId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // Method to delete a product by ID
    public void deleteProductById(int productId) {
        String sql = "DELETE FROM products WHERE product_id = ?";

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setInt(1, productId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
